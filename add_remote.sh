#!/usr/bin/env bash

set -euxo pipefail

# Add the obspm remote if it does not exist
conan remote list | grep obspm || conan remote add obspm https://conan.obspm.fr/conan --insecure
