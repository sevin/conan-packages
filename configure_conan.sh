#!/usr/bin/env bash

set -euxo pipefail

# Install conan if not found
command -v conan || python -m pip install conan

conan_home=$(conan config home)

# Create the default conan profile
conan profile detect --force

# Remove the cppstd settings. It does not add any value.
sed -i '/compiler.cppstd/d' $conan_home/profiles/default

# Disable unit test by default for all packages. Test can be re-enabled on a per package basis using `-c tools.build:skip_test=False`
grep -q 'tools.build:skip_test' $conan_home/global.conf || echo 'tools.build:skip_test = True' >> $conan_home/global.conf
