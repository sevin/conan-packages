from conan import ConanFile
from conan.tools.build import check_min_cppstd
from conan.tools.files import copy, get
from conan.tools.layout import basic_layout
import os

required_conan_version = ">=1.50.0"


class Jsoncons(ConanFile):
    name = "jsoncons"
    homepage = "https://github.com/danielaparker/jsoncons"
    description = "A C++, header-only library for constructing JSON and JSON-like data formats."
    topics = "json", "header-only", "cbor", "bson", "ubjson"
    url = "https://github.com/conan-io/conan-center-index"
    license = "MIT"
    settings = "os", "arch", "compiler", "build_type"
    no_copy_source = True

    @property
    def _minimum_cpp_standard(self):
        return 11

    def layout(self):
        basic_layout(self, src_folder="src")

    def package_id(self):
        self.info.clear()

    def validate(self):
        if self.settings.compiler.cppstd:
            check_min_cppstd(self, self._minimum_cpp_standard)

    def source(self):
        get(self, **self.conan_data["sources"][self.version],
            destination=self.source_folder, strip_root=True)

    def generate(self):
        pass

    def build(self):
        pass

    def package(self):
        copy(self, "LICENSE*", self.source_folder, os.path.join(self.package_folder, "licenses"))
        copy(self, "*", os.path.join(self.source_folder, "include"), os.path.join(self.package_folder, "include"))

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "jsoncons")
        self.cpp_info.set_property("cmake_target_name", "jsoncons")
        self.cpp_info.set_property("pkg_config_name", "jsoncons")
        self.cpp_info.bindirs = []
        self.cpp_info.libdirs = []
