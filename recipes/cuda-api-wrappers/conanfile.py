from conan import ConanFile
from conan.tools.cmake import CMake
from conan.tools.files import get

class CudaApiWrappers(ConanFile):
    name = 'cuda-api-wrappers'
    license = 'BSD-3-Clause'
    author = 'Eyal Rozenberg eyalroz@alumni.technion.ac.il'
    url = 'https://github.com/eyalroz/cuda-api-wrappers'
    description = 'Thin C++-flavored wrappers for the CUDA Runtime API.'

    settings = 'os', 'compiler', 'build_type', 'arch'
    no_copy_source = True
    generators = 'CMakeDeps', 'CMakeToolchain'

    # @property
    # def _source_subfolder(self):
    #     return "source_subfolder"


    def source(self):
        get(self, **self.conan_data["sources"][self.version], strip_root=True)


    def build(self):
        cmake = CMake(self)

        cmake.configure()
        # cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_id(self):
        self.info.clear()