from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import get, rm, apply_conandata_patches, export_conandata_patches
from conan.tools.scm import Git
import os
from copy import deepcopy

required_conan_version = ">=1.53.0"


class Milk(ConanFile):
    name = 'milk'
    description = 'Image processing functions, optimized for execution speed and high speed I/O'
    url = 'https://github.com/milk-org/milk'
    homepage = 'https://milk-org.github.io/milk/'
    topics = 'real-time' 'fps' 'astronomy' 'image-processing' 'image-analysis' 'processinfo'
    license = 'BSD-3-Clause'
    settings = 'os', 'arch', 'compiler', 'build_type'

    # Cannot be optional (link to the use of cuda or not).
    python_requires = 'conan_cuda/1.0.0'

    def export_sources(self):
        export_conandata_patches(self)
    
    def layout(self):
        cmake_layout(self, src_folder="src")

    def source(self):
        # we recover the saved url and commit from conandata.yml and use them to get sources
        git = Git(self)
        sources = self.conan_data["sources"][self.version]
        self.output.info(f"Cloning sources from: {sources}")
        git.clone(url=sources["url"], target=".")
        git.checkout(commit=sources["commit"])
        
        sources = self.conan_data["sources"]["image_stream_io/"+self.version]
        self.output.info(f"Cloning sources from: {sources}")
        git.clone(url=sources["url"], target="src/ImageStreamIO")
        git.folder = "src/ImageStreamIO"                       # cd target
        git.checkout(commit=sources["commit"])
        
    def generate(self):

        tc = CMakeToolchain(self)

        tc.variables['build_python_module'] = False
        tc.variables['USE_CUDA'] = True

        tc.generate()

        deps = CMakeDeps(self)
        deps.generate()
        
    def build(self):
        git = Git(self)
        git.folder = self.source_folder
        git.run('apply --ignore-whitespace %s' % os.path.join(self.export_sources_folder, "patches", "00_customize_milk.patch"))
        git.run("apply --ignore-whitespace --directory=src/ImageStreamIO %s" % os.path.join(self.export_sources_folder, "patches", "01_customize_ISIO.patch"))
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        # Install headers and cmake files.
        cmake.install()

    def package_info(self):
        self.cpp_info.components['CLIcore'].libs = [
            'milkCOREMODarith',
            'milkCOREMODmemory',
            'CLIcore',
            'milkCOREMODiofits',
            'milkCOREMODtools'
        ]
        self.cpp_info.components['CLIcore'].includedirs += [
            'include/CommandLineInterface'
        ]

        self.cpp_info.components['ImageStreamIO'].libs = [
            'ImageStreamIO'
        ]
        self.cpp_info.components['ImageStreamIO'].includedirs += [
            'include/ImageStreamIO'
        ]

        cuda_prop = self.python_requires['conan_cuda'].module.properties()

        self.cpp_info.components['ImageStreamIO'].defines = ['HAVE_CUDA']
        self.cpp_info.components['ImageStreamIO'].system_libs = ['cuda', 'cudart']
        self.cpp_info.components['ImageStreamIO'].libdirs += [cuda_prop.library]
        self.cpp_info.components['ImageStreamIO'].includedirs += [cuda_prop.include]

