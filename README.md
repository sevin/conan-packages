# Conan packages

Conan packages (CP) is a collection of conan packages and utility script.

## Installation

The following will install conan, configure it, add the cosmic repo and export packages to the local cache.

```bash
curl -sS https://gitlab.obspm.fr/jbernard/conan-packages/-/raw/main/install.sh | bash
```

Or, if you prefer to do it manually:

```bash
# Get sources
git clone git@gitlab.obspm.fr:jbernard/conan_packages.git
# Configure conan
./conan_packages/configure_conan.sh
# Add cosmic repo
./conan_packages/add_remote.sh
# Install packages
./conan_packages/install_pkg.sh
```

See [https://docs.conan.io/2](https://docs.conan.io/2) for more information concerning conan package manager.
