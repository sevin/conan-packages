#!/usr/bin/env bash

set -euxo pipefail

tmp_dir=$(mktemp -d -t conan-packages-XXXXXXXXXX)
git clone https://gitlab.obspm.fr/jbernard/conan-packages.git $tmp_dir --depth 1
trap 'rm -rf -- "$tmp_dir"' EXIT

$tmp_dir/configure_conan.sh
$tmp_dir/add_remote.sh
$tmp_dir/install_pkg.sh
