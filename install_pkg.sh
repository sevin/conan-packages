#!/usr/bin/env bash

set -euxo pipefail

repo_dir="$(realpath --relative-to=$(pwd) $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd ))"

# Clone a git repository in /tmp and pushd into it. The directory will be removed when the script exits.
function clone_pushd_tmp {
  tmp_dir=$(mktemp -d)
  git clone $1 $tmp_dir --depth 1 --no-single-branch
  trap 'rm -rf -- "$tmp_dir"' EXIT
  pushd $tmp_dir
  git fetch --tags
}

recipes=$repo_dir/recipes

conan export $recipes/conan_cuda --version=1.0.0

conan export $recipes/MatX --version=0.6.0
conan export $recipes/milk --version=1.3.20231122

# conan export $recipes/jsoncons --version=0.170.2.a

conan export $recipes/cuda-api-wrappers --version=0.6.1
conan export $recipes/cuda-api-wrappers --version=0.6.2
conan export $recipes/cuda-api-wrappers --version=0.6.3
conan export $recipes/cuda-api-wrappers --version=0.6.5.graph

# Install emu
clone_pushd_tmp https://gitlab.obspm.fr/cosmic/tools/emu.git
  git checkout rc1.0.0
  conan export .
  conan export python
popd
